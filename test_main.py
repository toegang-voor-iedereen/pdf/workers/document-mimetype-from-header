from unittest.mock import MagicMock, patch
import pytest

from main import jobHandler
from kimiworker import Worker, WorkerJob


@pytest.fixture
def mock_logger():
    return MagicMock()


@pytest.fixture
def mock_remote_storage():
    return MagicMock()


@pytest.fixture
def mock_publish():
    return MagicMock()


@pytest.fixture
def setup_mocks():
    # Mocks for the test
    mocked_log = MagicMock()
    mocked_job = {"bucketName": "test_bucket", "filename": "test.pdf"}
    mocked_publish = MagicMock()

    mocked_remote_file_storage = MagicMock()
    mocked_remote_file_storage.get_object.return_value = b"%PDF-1.4..."

    print("Set up mocks...")

    return mocked_log, mocked_job, mocked_remote_file_storage, mocked_publish


def test_job_handler_success(setup_mocks):
    mocked_log, mocked_job, mocked_remote_file_storage, mocked_publish = setup_mocks

    print("test_job_handler_success...")

    # Run the function with mocks
    jobHandler(
        mocked_log,
        mocked_job,
        job_id="123",
        local_file_path="/tmp/test.pdf",
        remote_file_storage=mocked_remote_file_storage,
        publish=mocked_publish,
    )

    # Asserts
    mocked_remote_file_storage.get_object.assert_called_once_with(
        "test_bucket", "test.pdf", offset=0, length=1024
    )
    assert "Got partial object, now detecting mimetype..." in str(
        mocked_log.info.call_args_list
    )
    mocked_publish.assert_called_once_with(
        "stringResult", "application/pdf", success=True, confidence=100
    )


def test_job_handler_no_mime_type(setup_mocks):
    mocked_log, mocked_job, mocked_remote_file_storage, mocked_publish = setup_mocks
    # Change mock to return empty MIME type
    mocked_remote_file_storage.get_object.return_value = b""

    # Run the function with altered mock
    jobHandler(
        mocked_log,
        mocked_job,
        job_id="123",
        local_file_path="/tmp/test.pdf",
        remote_file_storage=mocked_remote_file_storage,
        publish=mocked_publish,
    )

    # Asserts
    mocked_publish.assert_called_once_with(
        "stringResult", "application/x-empty", success=True, confidence=100
    )


@patch("magic.from_buffer")
def test_successful_mime_detection(
    mock_magic, mock_logger, mock_remote_storage, mock_publish
):
    mock_remote_storage.get_object.return_value = b"test data"
    mock_magic.return_value = "application/pdf"

    job: WorkerJob = {
        "bucketName": "test-bucket",
        "filename": "test.pdf",
        "recordId": "record123",
        "attributes": {},
    }

    jobHandler(
        mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish
    )

    mock_remote_storage.get_object.assert_called_with(
        "test-bucket", "test.pdf", offset=0, length=1024
    )
    mock_publish.assert_called_with(
        "stringResult", "application/pdf", success=True, confidence=100
    )


@patch("magic.from_buffer")
def test_failed_mime_detection(
    mock_magic, mock_logger, mock_remote_storage, mock_publish
):
    mock_remote_storage.get_object.return_value = b"test data"
    mock_magic.return_value = ""

    job: WorkerJob = {
        "bucketName": "test-bucket",
        "filename": "test.pdf",
        "recordId": "record123",
        "attributes": {},
    }

    jobHandler(
        mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish
    )

    mock_publish.assert_called_with(
        "stringResult", "text/plain", success=True, confidence=0
    )
    mock_logger.warn.assert_called_with("Failed to get mime type")
