import time
import random
from kimiworker import (
    KimiLogger,
    WorkerJob,
    PublishMethod,
    Worker,
    RemoteFileStorageInterface,
)


def mockedJobHandler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    log.info("Mocked job handler started")
    log.info(f"Record ID: {job.get('recordId')}")
    log.info(f"File Path: {local_file_path}")
    time.sleep(random.randint(5, 15))

    publish("stringResult", "application/pdf", success=True, confidence=100)


# TODO: Make more tests


def test_worker():
    worker = Worker(job_handler=mockedJobHandler, queue_name="worker-foo")
    assert worker.job_handler == mockedJobHandler
    assert worker.job_listener.amqp_host == "localhost"
    assert worker.job_listener.amqp_port == "1234"
    assert worker.job_listener.amqp_username == "amqp-user"
    assert worker.job_listener.amqp_password == "amqp-pass"
    assert worker.job_listener.exchange_prefix == "foo-exchange"
    assert worker.config.minio.host == "localhost"
    assert worker.config.minio.port == "1234"
    assert worker.config.minio.access_key == "minio-access-key"
    assert worker.config.minio.secret_key == "minio-secret-key"
