import random
import time
from unittest.mock import Mock, patch

from kimiworker import (
    KimiLogger,
    PublishMethod,
    RemoteFileStorageInterface,
    Worker,
    WorkerJob,
)


def mockedJobHandler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    log.info("Mocked job handler started")
    log.info(f"Record ID: {job.get('recordId')}")
    log.info(f"File Path: {local_file_path}")
    time.sleep(random.randint(5, 15))

    publish("stringResult", "application/pdf", success=True, confidence=100)


def test_worker(monkeypatch):
    monkeypatch.setenv("HOSTNAME", "test-hostname")
    monkeypatch.setenv("AMQP_HOST", "localhost")
    monkeypatch.setenv("AMQP_PORT", "1234")
    monkeypatch.setenv("AMQP_USER", "amqp-user")
    monkeypatch.setenv("AMQP_PASS", "amqp-pass")
    monkeypatch.setenv("EXCHANGE", "foo-exchange")
    monkeypatch.setenv("MINIO_HOST", "localhost")
    monkeypatch.setenv("MINIO_PORT", "1234")
    monkeypatch.setenv("MINIO_ACCESS_KEY", "minio-access-key")
    monkeypatch.setenv("MINIO_SECRET_KEY", "minio-secret-key")

    with patch("kimiworker.worker.Minio") as mock_minio_class:
        mock_minio = Mock()
        mock_minio_class.return_value = mock_minio

        with patch("kimiworker.worker.JobListener"):
            worker = Worker(job_handler=mockedJobHandler, queue_name="worker-foo")

            assert worker.job_handler == mockedJobHandler
            assert worker.config.minio.host == "localhost"
            assert worker.config.minio.port == "1234"
            assert worker.config.minio.access_key == "minio-access-key"
            assert worker.config.minio.secret_key == "minio-secret-key"
            assert worker.config.amqp.host == "localhost"
            assert worker.config.amqp.port == "1234"
            assert worker.config.amqp.username == "amqp-user"
            assert worker.config.amqp.password == "amqp-pass"
            assert worker.config.amqp.exchange == "foo-exchange"
            assert worker.config.hostname == "test-hostname"

            mock_minio_class.assert_called_once_with(
                endpoint="localhost:1234",
                access_key="minio-access-key",
                secret_key="minio-secret-key",
                secure=True,
                cert_check=False,
            )
