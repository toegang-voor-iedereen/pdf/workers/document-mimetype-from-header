# Document Mimetype from Header worker

This python3 worker gets the mimetype from the document file header

## Running this worker locally

To run this worker locally on an MacOS device follow the following steps:

- Run `brew install libmagic`
- Run `pip install -r requirements.txt`
- Run `AMQP_HOST="127.0.0.1" AMQP_PORT=5672 AMQP_USER="YOUR_AMQP_USER" AMQP_PASS="YOUR_AMQP_PASS" EXCHANGE="pageattribute-text-worker" MINIO_HOST="127.0.0.1" MINIO_PORT=9000 MINIO_ACCESS_KEY="YOUR_MINIO_ACCESS_KEY" MINIO_SECRET_KEY="YOUR_MINIO_SECRET_KEY" python3 main.py`