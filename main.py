import sys
import os
import magic
from kimiworker import PublishMethod, Worker, WorkerJob, RemoteFileStorageInterface

number_of_concurrent_jobs = int(os.getenv("CONCURRENT_JOBS", 1))
bytes_to_download = 1024


def jobHandler(
    log,
    job: WorkerJob,
    job_id,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    bucket_name = job.get("bucketName")
    filename = job.get("filename")

    log.info(
        f"Getting {bytes_to_download}b of data from bucket: {bucket_name} | filename: {filename}"
    )
    partial_object_bytes = remote_file_storage.get_object(
        bucket_name, filename, offset=0, length=bytes_to_download
    )

    log.info("Got partial object, now detecting mimetype...")
    mime = magic.from_buffer(partial_object_bytes, mime=True)
    log.info(f"Found: {mime}")

    if mime != "":
        log.info("Job done")
        publish("stringResult", mime, success=True, confidence=100)
    else:
        log.warn("Failed to get mime type")
        publish("stringResult", "text/plain", success=True, confidence=0)


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(
            jobHandler,
            queue_name="worker-document-mimetype-python",
            auto_download=False,
        )
        worker.start(number_of_concurrent_jobs)

    except KeyboardInterrupt:  # pragma: no cover
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
