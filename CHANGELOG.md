## [1.20.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.20.0...1.20.1) (2025-01-20)


### Bug Fixes

* update base worker ([6225fc2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/6225fc279ccbb5e9cb6f2195a88896bbce2a91b4))

# [1.20.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.19.0...1.20.0) (2024-12-17)


### Bug Fixes

* use new minio config in test ([ba4e80a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/ba4e80ab33ef7345ee8d0b79372889b563938c96))


### Features

* **deps:** kimiworker 4.4.0 ([0016ca2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/0016ca2ed9e30004fa8b5a0e9622c48780f69873))

# [1.19.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.18.0...1.19.0) (2024-12-11)


### Features

* **deps:** kimiworker v4.1.0, updated debug filename ([7f2fe5e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/7f2fe5ecb802dd5d2a09b4581419522be0fb9ced))

# [1.18.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.17.0...1.18.0) (2024-12-11)


### Features

* use kimiworker 4.0.0 with remote file storage, removed minio dependency ([5726d78](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/5726d78d0ac134eb5ee4d1afc4209dbe0b5718b7))

# [1.17.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.16.0...1.17.0) (2024-11-19)


### Features

* kimiworker v3.6.0 ([873ef6d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/873ef6db36f47c09a3b0a75771fed91e1c53cc2a))

# [1.16.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.15.0...1.16.0) (2024-11-12)


### Features

* force new release ([556df8d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/556df8d23a8f99cb1d624ef88b8fcf90d5442a0f))

# [1.15.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.14.0...1.15.0) (2024-11-06)

### Features

- kimiworker 3.5.0 ([5bb338f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/5bb338f6a77bfaab5ed8e6b9fdbfa2bb9bb9e521))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.13.0...1.14.0) (2024-11-06)

### Bug Fixes

- removed x ([8ebd1f5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/8ebd1f5f061ce479fa1868e02355aba7e066aaeb))
- use kimiworker 3.3.1 to disable cert check ([3c76d74](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/3c76d74ca25bfa31f07b6f850ca904dab252e1d8))

### Features

- use kimiworker 3.3.0 which connects to MinIO over HTTPS ([3d7e385](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/3d7e385c5130f11062a1aca7c4dbb6c9181c5154))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.12.0...1.13.0) (2024-11-05)

### Features

- kimiworker v3.3.0 ([f940a36](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/f940a36b4641192e4f41d18a9f6d9f4ea9176090))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.11.0...1.12.0) (2024-10-22)

### Features

- use kimiworker 2.15.0 with nldocspec 4.1.1 ([2f26ff5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/2f26ff5d6f97f91c380a3888cfe1893eda5f375b))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.10.0...1.11.0) (2024-09-11)

### Features

- added volume mount for /tmp ([64f5278](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/64f527893dbc0445f990f421c0857fe25502108d))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.9.0...1.10.0) (2024-09-04)

### Features

- added securityContext to container and initContainers ([c340f1a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/c340f1a40e81e880206eab34163b01d2b328bf99))
- use kimiworker 2.6.3 ([d7ec897](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/d7ec897b94aa320706d19f58c38e44bb4265b871))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.8.0...1.9.0) (2024-08-28)

### Features

- use new kimiworker to connect to station 4.0.0 and up ([2f4ffe8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/2f4ffe89489a1b79e648f6d9fcd64470949e9a07))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.7.0...1.8.0) (2024-07-04)

### Features

- support initContainers in helm values ([4c4566e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/4c4566e0a6f1b96723fc87d66934e7230bc2b558))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.6.0...1.7.0) (2024-06-05)

### Features

- support custom initContainers in helm values ([1853c4f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/1853c4f95e08ee9255ab206d57bf54eef98c1685))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.5.2...1.6.0) (2024-06-04)

### Features

- correct worker name in chart ([4ef5aaa](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/4ef5aaaf3ee0fb5731bf75740a8abcc52371a0ff))

## [1.5.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/1.5.1...1.5.2) (2024-05-13)

### Bug Fixes

- compatibility ([26b773f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/26b773f4df50787e2f27a1445670765f4aa352d8))

## [1.5.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.5.0...1.5.1) (2024-04-24)

### Bug Fixes

- force a release with the new pipeline ([ab35cc6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/ab35cc64b846565da7380bdac704367259341fac))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.4.3...v1.5.0) (2024-04-17)

### Features

- use kimiworker 1.2.1 ([b38c205](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/b38c205416f688c4ec96c1b0c3eb08a03f873cf6))

## [1.4.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.4.2...v1.4.3) (2024-04-04)

### Bug Fixes

- update Chart.yaml ([90b8188](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/90b8188cbce94606282bd9039a91a1bf5af62330))
- using group id variable instead of literal in gitlab ci file ([e2d160c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/e2d160c8188b586ebd450bdd81e7152dd08d5890))

## [1.4.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.4.1...v1.4.2) (2024-04-03)

### Bug Fixes

- removed comments from Chart.yaml ([be61bc3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/be61bc32485fc24d0bdacc67d9d4f8d296bd891b))
- update Chart.yaml ([43bf013](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/43bf013c9f72bed7d62df47d05d0662bf95b005f))

## [1.4.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.4.0...v1.4.1) (2024-04-03)

### Bug Fixes

- added coverage reports to gitignore ([5db7368](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/5db7368f8c8c293bbb0e1ffae6dbf0ebb776a8ee))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.3.2...v1.4.0) (2024-04-03)

### Features

- cleaned up imports (forcing release) ([e9394da](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/e9394da43b6b5ce2ae7f645d9fb6753364a956b0))

## [1.3.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.3.1...v1.3.2) (2024-04-03)

### Bug Fixes

- added releaserc to .gitignore ([11f8860](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/11f88603295e551d93db9b84223ea3facfb51e74))

## [1.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/compare/v1.3.0...v1.3.1) (2024-03-28)

### Bug Fixes

- added todo ([aaec993](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header/commit/aaec993a0385d9cf4208fe4086249bfdb3a605ff))
